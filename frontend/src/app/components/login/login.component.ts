import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserControllerService } from 'src/app/openapi';
import { TokenserviceService } from 'src/app/services/tokenservice.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  fb: FormGroup;
  
  constructor(private fgb: FormBuilder,
    private controllerUser:UserControllerService,
    private tokerservice: TokenserviceService,
    private activeRouter: Router) {
    this.init();
   }

  ngOnInit(): void {
  }

  login(){

    if (this.fb.valid){
      //Objeto necesario del schema
      const request = {
        email:this.fb.value.email,
        password:this.fb.value.password
      }

      this.controllerUser.userControllerLogin(request).subscribe((response)=>{
        this.tokerservice.saveToken(response.token);
        this.activeRouter.navigateByUrl('/');
      })

    }
  }

  init(){
    this.fb = this.fgb.group({
      email:['', Validators.required],
      password:['', Validators.required]
    })
  }

}
