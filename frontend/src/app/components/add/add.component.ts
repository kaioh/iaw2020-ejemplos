import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UseritemItemControllerService } from 'src/app/openapi';
import { TokenserviceService } from 'src/app/services/tokenservice.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  fb: FormGroup;
  
  constructor(private fgb: FormBuilder,
    private tokerservice:TokenserviceService, 
    private userController:UseritemItemControllerService,
    private activeRouter: Router) { 
      this.init();
    }

  init(): void{
    this.fb = this.fgb.group({
      name: ['', Validators.required ]
    });
  }
  
  ngOnInit(): void {
  }

  save(): void{
    if (this.fb.valid){
      const user = this.tokerservice.getUser();
      this.userController.useritemItemControllerCreate(user.id,
        { name: this.fb.value.name,
          useritemId: user.id
        }).subscribe((response)=>{
          this.activeRouter.navigateByUrl('/');
        })
    }
  }

}
