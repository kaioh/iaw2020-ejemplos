import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserModel } from 'src/app/model/user.model';
import { TokenserviceService } from 'src/app/services/tokenservice.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  user: UserModel;

  constructor(private tokenservice: TokenserviceService,private activeRouter: Router) { }

  ngOnInit(): void {

  }

  isLogin(): boolean {
    if (this.tokenservice.isValid()){
      this.user = this.tokenservice.getUser();
    }
    return this.tokenservice.isValid();
  }

  salir(){
    this.tokenservice.signOut();
    this.activeRouter.navigateByUrl('/home');
  }

}
