import { Component, OnInit } from '@angular/core';
import { ItemModel } from 'src/app/model/item.model';
import { UseritemItemControllerService } from 'src/app/openapi';
import { TokenserviceService } from 'src/app/services/tokenservice.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  listItems: ItemModel[] = [];

  constructor(private token:TokenserviceService,
    private controllerUser:UseritemItemControllerService) {

  }

  ngOnInit(): void {
    this.load();
  }

  load(){

    if (this.token.isValid()){

      this.controllerUser.configuration.accessToken = this.token.getToken();

      const user = this.token.getUser();

      this.controllerUser.useritemItemControllerFind(user.id) .subscribe((items)=>{
        items.map(item => {
          const n = new ItemModel();
          n.setName(item.name);
          n.setId(item.id);
          this.listItems.push(n);
        })
      });

    }
  }

  isLogin(){
    return this.token.isLogin();
  }

}
