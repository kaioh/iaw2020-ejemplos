import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserControllerService } from 'src/app/openapi';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  fb: FormGroup;
  
  constructor(private fgb: FormBuilder,private activeRouter: Router,
    private controllerUser:UserControllerService) { }

  ngOnInit(): void {
    this.init();
  }

  init(){
    this.fb = this.fgb.group({
      email:['', Validators.required],
      name:['', Validators.required],
      password:['', Validators.required]
    })
  }

  register(): void{

    if (this.fb.valid){

      const register = {
        name: this.fb.value.name,
        email: this.fb.value.email,
        password: this.fb.value.password,
        roles: ['customer']
      }

      this.controllerUser.userControllerSignUp(register).subscribe((response)=>{
        console.log("REGISTRADO");
        this.activeRouter.navigateByUrl('/');
      })



    }
  }

}
