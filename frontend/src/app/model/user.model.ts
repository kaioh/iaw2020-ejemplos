import { Useritem } from '../openapi';

export class UserModel implements Useritem {
    
    name: string;
    email: string;
    id?: string;
    roles?: Array<string>;

    constructor(){

    }

    setName(n){
        this.name = n;
    }
    setEmail(e){
        this.email = e;
    }

    setId(i){
        this.id = i;
    }
    setRoles(r){
        this.roles = r;
    }
}