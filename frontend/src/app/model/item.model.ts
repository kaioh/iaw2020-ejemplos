
import { Item } from '../openapi';

export class ItemModel implements Item {
    
    name: string;
    id?: string;
    useritemId?: string;

    constructor(){

    }

    setName(n:string){
        this.name=n;
    }

    getName(){
        return this.name;
    }

    setId(i){
        this.id = i;
    }

    setUserId(id){
        this.useritemId=id;
    }
}