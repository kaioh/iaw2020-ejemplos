import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { NavbarComponent } from './components/share/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { ReactiveFormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';
import { SignupComponent } from './components/signup/signup.component';


//Api service loopback
import { ApiModule } from './openapi/api.module';
import { TokenserviceService } from './services/tokenservice.service';
import { AddComponent } from './components/add/add.component';
import { Sampleguard } from './services/sampleguard';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavbarComponent,
    HomeComponent,
    SignupComponent,
    AddComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    ApiModule
  ],
  providers: [TokenserviceService,Sampleguard],
  bootstrap: [AppComponent]
})
export class AppModule { }
