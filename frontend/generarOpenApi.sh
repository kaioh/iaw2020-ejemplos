#!/bin/bash

rm -rf ./src/app/openapi

openapi-generator-cli generate -g typescript-angular -i http://localhost:3000/openapi.json -o ./src/app/openapi
