import {authenticate} from '@loopback/authentication';
import {authorize} from '@loopback/authorization';
import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {Item, Useritem} from '../models';
import {UseritemRepository} from '../repositories';
import {checkAuthorization} from '../services/checkauthorization';

export class UseritemItemController {
  constructor(
    @repository(UseritemRepository)
    protected useritemRepository: UseritemRepository,
  ) {}

  @get('/useritems/{id}/items', {
    responses: {
      '200': {
        description: 'Array of Useritem has many Item',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Item)},
          },
        },
      },
    },
  })
  async find(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<Item>,
  ): Promise<Item[]> {
    return this.useritemRepository.items(id).find(filter);
  }

  @authenticate('jwt')
  @authorize({
    allowedRoles: ['customer'],
    resource: 'UseritemItemController.prototype.create',
    voters: [checkAuthorization],
  })
  @post('/useritems/{id}/items', {
    responses: {
      '200': {
        description: 'Useritem model instance',
        content: {'application/json': {schema: getModelSchemaRef(Item)}},
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof Useritem.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Item, {
            title: 'NewItemInUseritem',
            exclude: ['id'],
            optional: ['useritemId'],
          }),
        },
      },
    })
    item: Omit<Item, 'id'>,
  ): Promise<Item> {
    return this.useritemRepository.items(id).create(item);
  }

  @patch('/useritems/{id}/items', {
    responses: {
      '200': {
        description: 'Useritem.Item PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Item, {partial: true}),
        },
      },
    })
    item: Partial<Item>,
    @param.query.object('where', getWhereSchemaFor(Item)) where?: Where<Item>,
  ): Promise<Count> {
    return this.useritemRepository.items(id).patch(item, where);
  }

  @del('/useritems/{id}/items', {
    responses: {
      '200': {
        description: 'Useritem.Item DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.string('id') id: string,
    @param.query.object('where', getWhereSchemaFor(Item)) where?: Where<Item>,
  ): Promise<Count> {
    return this.useritemRepository.items(id).delete(where);
  }
}
