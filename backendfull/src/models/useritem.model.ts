import {Entity, hasMany, model, property, hasOne} from '@loopback/repository';
import {Item} from './item.model';
import {UserCredentials} from './user-credentials.model';

@model()
export class Useritem extends Entity {
  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
    required: true,
  })
  email: string;

  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'array',
    itemType: 'string',
  })
  roles?: string[];

  @hasMany(() => Item)
  items: Item[];

  @hasOne(() => UserCredentials)
  userCredentials: UserCredentials;

  constructor(data?: Partial<Useritem>) {
    super(data);
  }
}

export interface UseritemRelations {
  // describe navigational properties here
}

export type UseritemWithRelations = Useritem & UseritemRelations;
