import {AuthenticationComponent} from '@loopback/authentication';
import {
  JWTAuthenticationComponent,
  TokenServiceBindings
} from '@loopback/authentication-jwt';
import {AuthorizationComponent, AuthorizationTags} from '@loopback/authorization';
import {BootMixin} from '@loopback/boot';
import {ApplicationConfig} from '@loopback/core';
import {RepositoryMixin} from '@loopback/repository';
import {RestApplication} from '@loopback/rest';
import {
  RestExplorerBindings,
  RestExplorerComponent
} from '@loopback/rest-explorer';
import {ServiceMixin} from '@loopback/service-proxy';
import path from 'path';
import {DbsampleDataSource} from './datasources';
import {CustomAuthorizeProvider, PasswordHasherBindings, UserServiceBindings} from './keys';
import {MySequence} from './sequence';
import {Customauthorize} from './services/custom-authorize';
import {BcryptHasher} from './services/hash.password.bcryptjs';
import {JWTCustomService} from './services/jwtcustomservice';
import {MyUserService} from './services/user.service';

export {ApplicationConfig};

export class BackendfullApplication extends BootMixin(
  ServiceMixin(RepositoryMixin(RestApplication)),
) {
  constructor(options: ApplicationConfig = {}) {
    super(options);

    // Set up the custom sequence
    this.sequence(MySequence);

    // Set up default home page
    this.static('/', path.join(__dirname, '../public'));

    // Customize @loopback/rest-explorer configuration here
    this.configure(RestExplorerBindings.COMPONENT).to({
      path: '/explorer',
    });
    this.component(RestExplorerComponent);

    //auth componente
    this.component(AuthenticationComponent);
    // Mount jwt component
    this.component(JWTAuthenticationComponent);

    this.component(AuthorizationComponent);

    this.setUpBindings();

    this.dataSource(DbsampleDataSource, UserServiceBindings.DATASOURCE_NAME);

    this.projectRoot = __dirname;
    // Customize @loopback/boot Booter Conventions here
    this.bootOptions = {
      controllers: {
        // Customize ControllerBooter Conventions here
        dirs: ['controllers'],
        extensions: ['.controller.js'],
        nested: true,
      },
    };
  }

  setUpBindings(): void {
    // Bind bcrypt hash services
    this.bind(PasswordHasherBindings.ROUNDS).to(10);
    this.bind(PasswordHasherBindings.PASSWORD_HASHER).toClass(BcryptHasher);
    this.bind(TokenServiceBindings.TOKEN_SERVICE).toClass(JWTCustomService);
    this.bind(UserServiceBindings.USER_SERVICE).toClass(MyUserService);
    this.bind(CustomAuthorizeProvider.AUTHORIZE_PROVIDER)
      .toProvider(Customauthorize).tag(AuthorizationTags.AUTHORIZER);

  }
}
